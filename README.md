# homeauto

Scripts for automating stuff in my house. I am using Domoticz as my automation hub so most of the scripts are in DzVents.
To get an idea how I am using them, visit my [blog](https://anonimno.codeberg.page).